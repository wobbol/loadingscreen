/* Copyright 2018 - 2020 Amine B. Hassouna amine.benhassouna@gmail.com */
/* Copyright 2023 N.F. nfontenot27@gmail.com */
/* MIT Licence */
#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <time.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <ini.h>

#define LOADING_SCREEN_LIMIT 20
#define NAME_SZ 64
#define PATH_SZ 512

struct loadingscreen_t {
	char name[NAME_SZ];
	char spinner_image_path[PATH_SZ];
	char background_image_path[PATH_SZ];
	char music_path[PATH_SZ];
	float spinner_speed; //TODO: make this based on time per rotation
	int spinner_location_x;
	int spinner_location_y;
	float background_scale;
	float spinner_scale;
	int window_height;
	int window_width;
};

struct settings_t {
	int len;
	struct loadingscreen_t ls[LOADING_SCREEN_LIMIT];
	char display[NAME_SZ];
};
int fprintf_settings(FILE *file, struct settings_t s) {
	int count = 0;
	if(s.len >= LOADING_SCREEN_LIMIT) {
		return -1; /* corrupt settings */
	}
	count += fprintf(file, "display= '%s'\n", s.display);
	for(int i = 0; i < s.len; ++i) {
		count += fprintf(file,
		"\n[%s]\n"
		"spinner_image_path = %s\n"
		"background_image_path = %s\n"
		"music_path = %s\n"
		"spinner_speed = %10.10f\n"
		"spinner_location_x = %d\n"
		"spinner_location_y = %d\n"
		"background_scale = %f\n"
		"spinner_scale = %f\n"
		"window_height = %d\n"
		"window_width = %d\n",
				s.ls[i].name,
				s.ls[i].spinner_image_path,
				s.ls[i].background_image_path,
				s.ls[i].music_path,
				s.ls[i].spinner_speed,
				s.ls[i].spinner_location_x,
				s.ls[i].spinner_location_y,
				s.ls[i].background_scale,
				s.ls[i].spinner_scale,
				s.ls[i].window_height,
				s.ls[i].window_width);
	}
	return count;
}
int parse_int_with_error(const char *const number, int *out) {
		char *e;
		*out = strtol(number, &e, 10);
		if(*out == 0 && e == number) {
			printf("didn't find a number in '%s'\n", number);
			return -1;
		}
		return 0;
}
int parse_float_with_error(const char *const number, float *out) {
		char *e;
		*out = strtof(number, &e);
		if(*out == 0 && e == number) {
			printf("didn't find a number in '%s'\n", number);
			return -1;
		}
		return 0;
}
int config_handler(void *user, const char *section, const char *name, const char *value) {
	struct settings_t *settings = user;
	struct loadingscreen_t *ls = settings->ls;
	int found = 0;
	int ls_off;

	if(section[0] == '\0') { /* outside section heading */
		if(strcmp("display", name) == 0) {
			strncpy(settings->display, value, sizeof(settings->display));
			return 1; /* Success */
		} else {
			printf("unknown property name\n");
			return 0; /* Failure */
		}
	}

	for(ls_off = 0; ls_off < LOADING_SCREEN_LIMIT; ++ls_off) {
		if(strcmp(ls[ls_off].name, section) == 0) {
			found = 1;
			break;
		}
	}
	if(found) {
		ls += ls_off;
	} else {
		if((settings->len) < LOADING_SCREEN_LIMIT) {
			ls_off = settings->len;
			++settings->len;
			ls += ls_off;
			strncpy(ls->name, section, sizeof(ls->name));
			ls->name[sizeof(ls->name)-1] = '\0';
		} else {
			printf("too many loadingscreen sections\n");
			return 0; /* Failure */
		}
	}
	//assert ls == current loading screen section
	//assert ls_off == difference between ls and settings->ls
	//assert ls->name == section
	
	if(strcmp("spinner_image_path", name) == 0) {
		strncpy(ls->spinner_image_path, value, sizeof(ls->spinner_image_path));
	} else if(strcmp("background_image_path", name) == 0) {
		strncpy(ls->background_image_path, value, sizeof(ls->background_image_path));
	} else if(strcmp("music_path", name) == 0) {
		strncpy(ls->music_path, value, sizeof(ls->music_path));
	} else if(strcmp("spinner_speed", name) == 0) {
		if(parse_float_with_error(value, &ls->spinner_speed) != 0) {
			return 0; /* Failure */
		}
	} else if(strcmp("spinner_location_x", name) == 0) {
		if(parse_int_with_error(value, &ls->spinner_location_x) != 0) {
			return 0; /* Failure */
		}
	} else if(strcmp("spinner_location_y", name) == 0) {
		if(parse_int_with_error(value, &ls->spinner_location_y) != 0) {
			return 0; /* Failure */
		}
	} else if(strcmp("background_scale", name) == 0) {
		if(parse_float_with_error(value, &ls->background_scale) != 0) {
			return 0; /* Failure */
		}
	} else if(strcmp("spinner_scale", name) == 0) {
		if(parse_float_with_error(value, &ls->spinner_scale) != 0) {
			return 0; /* Failure */
		}
	} else if(strcmp("window_height", name) == 0) {
		if(parse_int_with_error(value, &ls->window_height) != 0) {
			return 0; /* Failure */
		}
	} else if(strcmp("window_width", name) == 0) {
		if(parse_int_with_error(value, &ls->window_width) != 0) {
			return 0; /* Failure */
		}
	} else {
		printf("unknown property name\n");
		return 0; /* Failure */
	}
	return 1; /* Success */
}
struct loadingscreen_t *select_loadingscreen(const char *const name, struct settings_t *s) {
	if(s->len >= LOADING_SCREEN_LIMIT) {
		printf("corrupt settings\n");
		return s->ls;
	}
	for(int i = 0; i < s->len; ++i) {
		if(strncmp(name, s->ls[i].name, sizeof(s->ls[i].name)) == 0) {
			return s->ls + i;
		}
	}
	printf("loadingscreen not found\n");
	return s->ls;
}

long long signed timespec_to_usec(struct timespec *t) {
	return t->tv_sec * 1000000 + t->tv_nsec / 1000;
}

struct spinner_t {
	SDL_Rect rect;
	SDL_Rect full_size;
	SDL_Texture *tex;
	SDL_RendererFlip flip;
	float speed;
};
void print_timeval(struct timeval *t, char *name) {
	printf("%s %10.10d %8.6d\n", name, t->tv_sec, t->tv_usec);
}

void update_spinner_state(struct spinner_t *spinner, struct timespec *start, struct timespec *now) {
	long long signed pos_us, speed_us, diff_us, now_us, start_us;
	double iptr, frac;

	/* 1000000 usec / 1 sec */
	frac = modf(spinner->speed, &iptr);
	speed_us = iptr * 1000000 + frac * 1000000;
	if(speed_us == 0) {
		spinner->rect.x = spinner->full_size.x;
		spinner->rect.w = spinner->full_size.w;
		return;
	}

	now_us = timespec_to_usec(now);
	start_us = timespec_to_usec(start);
	diff_us = now_us - start_us;
	pos_us = diff_us % speed_us;

	{
		int max_width = spinner->full_size.w;
		int center_x = spinner->full_size.x + (max_width/2.0);
		double pos = (double)pos_us / speed_us;
		double scale_w = sin((pos) * 2 * M_PI);
		assert(pos >= 0.0);
		assert(pos <= 1.0);

		spinner->rect.x = center_x - abs(max_width/2.0 * scale_w);
		spinner->rect.w = abs(max_width * scale_w);
		if(pos < 1.0/2.0) {
			spinner->flip = SDL_FLIP_NONE;
		} else {
			spinner->flip = SDL_FLIP_HORIZONTAL;
		}
	}
}

int sdl_error(char *fmt, ...) {
	int count;
	va_list ap;

	va_start(ap, fmt);
	count = vfprintf(stderr, fmt, ap);
	va_end(ap);

	count += fprintf(stderr, "\nSDL2: %s\n", SDL_GetError());
	return count;
}

int img_error(char *fmt, ...) {
	int count;
	va_list ap;

	va_start(ap, fmt);
	count = vfprintf(stderr, fmt, ap);
	va_end(ap);

	count += fprintf(stderr, "\nSDL2_IMG: %s\n", IMG_GetError());
	return count;
}

int mix_error(char *fmt, ...) {
	int count;
	va_list ap;

	va_start(ap, fmt);
	count = vfprintf(stderr, fmt, ap);
	va_end(ap);

	count += fprintf(stderr, "\nSDL2_Mix: %s\n", Mix_GetError());
	return count;
}

int main(void) {
	struct settings_t settings = {0};
	puts("parse ini");
	if(ini_parse("data/config.ini", config_handler, &settings) != 0) {
		printf("ini parse error\n");
		return 0;
	}
	fprintf_settings(stdout, settings);
	struct loadingscreen_t *ls = select_loadingscreen(settings.display, &settings);
	printf("loading %s loadingscreen\n", ls->name);

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
		sdl_error("Failed to initalize SDL2\n");
		return 0;
	}

	int flags =
	IMG_INIT_JPG  | IMG_INIT_PNG | IMG_INIT_TIF |
	IMG_INIT_WEBP | IMG_INIT_JXL | IMG_INIT_AVIF;

	puts("SDL_img init");
	if(IMG_Init(flags) == 0) {
		img_error("Failed to initialize SDL2_image\n");
		return 0;
	}

	puts("create window");
	SDL_Window *window = SDL_CreateWindow(
		ls->name,
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		ls->window_width,       ls->window_height,
		0);
	if(window == NULL) {
		sdl_error("Failed to create window\n");
		return 0;
	}


	puts("create renderer");
	SDL_Renderer *renderer = SDL_CreateRenderer(
		window,
		-1, /* index of the rendering driver to initalize */
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(renderer == NULL) {
		sdl_error("Failed to create renderer\n");
		return 0;
        }
	Mix_Music *ldMusic = NULL;
	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
		mix_error("Failed to initalize SDL_Mixer\n");
		/* non fatal */
	}
	ldMusic = Mix_LoadMUS(ls->music_path);
	if(ldMusic == NULL) {
		mix_error("Failed to load music\n");
		/* non fatal */
	}
	struct spinner_t spinner = {
		.rect = {0},
		.full_size = {0},
		.tex = NULL,
		.flip = SDL_FLIP_NONE,
		.speed = ls->spinner_speed};

	spinner.tex = IMG_LoadTexture(renderer, ls->spinner_image_path);
	if(spinner.tex == NULL) {
		img_error("Failed to load spinner image '%s'\n", ls->spinner_image_path);
		/* non fatal */
	}

	SDL_Texture *background = IMG_LoadTexture(renderer, ls->background_image_path);
	if(background == NULL) {
		img_error("Failed to load background image '%s'\n", ls->background_image_path);
		/* non fatal */
	}

	int res;
	res = SDL_QueryTexture(spinner.tex, NULL, NULL, &spinner.rect.w, &spinner.rect.h);
	if(res != 0) {
		sdl_error("Failed to get spinner height and width\n");
		/* non fatal */
	}
	spinner.rect.w *= ls->spinner_scale;
	spinner.rect.h *= ls->spinner_scale;
	spinner.rect.x = ls->spinner_location_x - spinner.rect.w / 2;
	spinner.rect.y = ls->spinner_location_y - spinner.rect.h / 2;

	SDL_Rect backgroundRect;
	res = SDL_QueryTexture(background, NULL, NULL, &backgroundRect.w, &backgroundRect.h);
	if(res != 0) {
		sdl_error("Failed to get background height and width\n");
		/* non fatal */
	}

	backgroundRect.w *= ls->background_scale;
	backgroundRect.h *= ls->background_scale;
	backgroundRect.x = 0;
	backgroundRect.y = 0;

	spinner.full_size = spinner.rect;

	struct timespec start;
	clock_gettime(CLOCK_MONOTONIC, &start);

	int quit = 0;
	while(!quit) {
		struct timespec now;
		clock_gettime(CLOCK_MONOTONIC, &now);

		if(Mix_PlayingMusic() == 0) {
			Mix_PlayMusic(ldMusic, -1);
		}

		update_spinner_state(&spinner, &start, &now);

		SDL_Event event;
		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT) {
				quit = 1;
			} 
			else if(event.type == SDL_KEYDOWN) {
				switch(event.key.keysym.sym) {
				case SDLK_q:
					quit = 1;
					break;
				}
			}
		}

		/* Magenta behind background */
		SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0xFF, 0xFF);
		SDL_RenderClear(renderer);

		SDL_RenderCopy(renderer, background, NULL, &backgroundRect);

		SDL_RenderCopyEx(renderer, spinner.tex, NULL, &spinner.rect,
			0, NULL, spinner.flip);

		SDL_RenderPresent(renderer);
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit();
	SDL_Quit();

	return 0;
}
